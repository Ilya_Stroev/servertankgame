package ru.inno.sockets.models;

/**
 * 23.12.2020
 * 41. Sockets
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Shot {
    private Player shooter;
    private Player target;
    private Game game;

    public Shot(Player shooter, Player target, Game game) {
        this.shooter = shooter;
        this.target = target;
        this.game = game;

    }

    public Player getShooter() {
        return shooter;
    }

    public Player getTarget() {
        return target;
    }

    public Game getGame() {
        return game;
    }
}
