package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Shot;

/**
 * 23.12.2020
 * 41. Sockets
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ShotsRepositoryImpl implements ShotsRepository {
    public void save(Shot shot) {
        System.out.println("ShootRepository - saved " + shot.getShooter().getNickName() + " "
                + shot.getTarget().getNickName() + " with game id = " + shot.getGame().getId());
    }
}
