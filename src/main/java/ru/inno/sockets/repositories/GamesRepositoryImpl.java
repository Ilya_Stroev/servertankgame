package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Game;

/**
 * 23.12.2020
 * 41. Sockets
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class GamesRepositoryImpl implements GamesRepository {

    // TODO: temp
    private Game game;

    @Override
    public void save(Game game) {
        System.out.println("Игра началась для игроков " + game.getFirst().getNickName() + " "
                + game.getSecond().getNickName() + " в " + game.getGameDate());
        game.setId(1L);
        this.game = game;
    }

    @Override
    public Game findById(Long gameId) {
        return game;
    }

    @Override
    public void update(Game game) {
        this.game.setFirstPlayerShotsCount(game.getFirstPlayerShotsCount());
        this.game.setSecondPlayerShotsCount(game.getSecondPlayerShotsCount());
    }
}
