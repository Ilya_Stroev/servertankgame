package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Shot;

/**
 * 23.12.2020
 * 41. Sockets
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ShotsRepository {
    void save(Shot shot);
}
