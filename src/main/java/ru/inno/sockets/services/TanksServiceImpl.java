package ru.inno.sockets.services;

import ru.inno.sockets.models.Game;
import ru.inno.sockets.models.Player;
import ru.inno.sockets.models.Shot;
import ru.inno.sockets.repositories.*;

import java.time.LocalDate;

/**
 * 23.12.2020
 * 41. Sockets
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class TanksServiceImpl implements TanksService {

    private ShotsRepository shotsRepository;
    private PlayersRepository playersRepository;
    private GamesRepository gamesRepository;

    public TanksServiceImpl(ShotsRepository shotsRepository, PlayersRepository playersRepository, GamesRepository gamesRepository) {
        this.shotsRepository = shotsRepository;
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
    }

    public Long startGame(String firstPlayerNickname, String secondPlayerNickname) {
        Player firstPlayer = playersRepository.findByNickname(firstPlayerNickname);
        Player secondPlayer = playersRepository.findByNickname(secondPlayerNickname);
        // если пользователи новые
        if (firstPlayer == null) {
            firstPlayer = new Player(firstPlayerNickname);
            playersRepository.save(firstPlayer);
        } else {
            // TODO: обновить IP
        }

        if (secondPlayer == null) {
            secondPlayer = new Player(secondPlayerNickname);
            playersRepository.save(secondPlayer);
        }else {
            // TODO: обновить IP
        }

        Game game = new Game();
        game.setFirst(firstPlayer);
        game.setSecond(secondPlayer);
        game.setGameDate(LocalDate.now().toString());

        gamesRepository.save(game);
        return game.getId();
    }

    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        Player shooter = playersRepository.findByNickname(shooterNickname);
        Player target = playersRepository.findByNickname(targetNickname);
        Game game = gamesRepository.findById(gameId);
        Shot shot = new Shot(shooter, target, game);
        if (game.getFirst().equals(shooter)) {
            game.setFirstPlayerShotsCount(game.getFirstPlayerShotsCount() + 1);
        } else if (game.getSecond().equals(shooter)) {
            game.setSecondPlayerShotsCount(game.getSecondPlayerShotsCount() + 1);
        }
        shotsRepository.save(shot);
        gamesRepository.update(game);
        System.out.println(" shots: " +
                game.getFirstPlayerShotsCount() + "|" + game.getSecondPlayerShotsCount());
    }
}
